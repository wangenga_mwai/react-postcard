import React from 'react';
import { shallow } from 'enzyme';
import Button from './Button';
import primaryTheme from '../../theme/primaryTheme';

test('should render the Button correctly', () => {
  const wrapper = shallow(<Button theme={primaryTheme}/>);
  expect(wrapper).toMatchSnapshot();
});

test('should render the Button with the primary theme', () => {
    const wrapper = shallow(<Button theme={primaryTheme}/>);
    expect(wrapper).toMatchSnapshot();
});
