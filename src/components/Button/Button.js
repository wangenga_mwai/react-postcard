import styled from 'styled-components';

const Button = styled.button`
    background: ${props => props.theme.colorBlue};
    border: none;
    text-transform: uppercase;
    border-radius: 2px;
    font-size: 10px;
    color: ${props => props.theme.colorWhite};
    outline: none;
    line-height: 11.72px;
    padding: 5px 6px;
    width: 108px;
    height: 30px;
    margin-bottom: 10px;
    cursor: pointer;
`;

export default Button;