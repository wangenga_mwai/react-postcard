import React, { useRef, useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import { addItem, updateCurrent, deleteItem } from '../../actions'
import styled from 'styled-components';

const Input = (props) => [
    <input 
      key={props.id}
      {...props.attr} 
      onChange={(evt) => props.onChange(evt.target.value)} 
      value={props.value} />
];

const EditableComponent = styled.div`
    color: ${props => props.theme.colorWhite};
    font-size: 24px;
    line-height: 28.13px;
    cursor: move;
    input {
        font-size: 24px;
        border:none;
    }
`
const EditableComponentWrapper = styled.div`
  width:100%;
  padding: 72px 10px 10px 10px;
`
const ContentEditable = (props) => {
    const {addNewItem,list, theme} = props
    const [position, setPosition] = useState({id:'', x: 0, y: 0})
    const [editMode, setEditMode] = useState(null)
    const [current, setCurrent] = useState({})
    const ref = useRef()
    const containerRef = useRef()
    const dispatch = useDispatch();

    let offsetX,offsetY

    useEffect(() => {
        if (ref.current) {
            ref.current.style.left = `${ref.current.pageX-offsetX}px`
            ref.current.style.top = `${ref.current.pageY-offsetY}px`
        }
    }, [position])
    useEffect(() => {
        handleAdd()
    }, [addNewItem]);

    const handleAdd = () => {
        if (!addNewItem || !list[0].text || list.length >= 5) {return}
        const addItemId = list.length + 1;
        const itemObject = { id: addItemId, text:''}
        dispatch(addItem(itemObject));
        setCurrent(itemObject);
        setEditMode(addItemId);
    }
    const handleDelete = (id) => {
        dispatch(deleteItem(id));
    }
    const handleChange = (obj) => {
        dispatch(updateCurrent(obj));
        setCurrent(obj)
    }
    const handleEdit = () => {
        const {text} = current;
        if (text) {
            dispatch(updateCurrent(current));
            setEditMode(null);
            setCurrent({})
        }
    }
    const handleDragStart = (e) => {
        const el= e.target
        offsetX= el.clientX-el.getBoundingClientRect().left
        offsetY= el.clientY-el.getBoundingClientRect().top
        setPosition({
            x: offsetX,
            y: offsetY
        })
    }
    const handleDragEnd = (e) => {
        const el= e.target.getBoundingClientRect()
        const container = containerRef.current.getBoundingClientRect()
        const verticalLimit = -Math.abs(el.height / 2)
        const horizontalLimit = -Math.abs(el.width / 2)

        switch (el, container) {
            case container.top - el.top < verticalLimit:
            case container.bottom - el.bottom < verticalLimit:
                handleDelete(current.id)
                break;
            case container.left - el.left < horizontalLimit:
            case container.right - el.right < horizontalLimit:
                handleDelete(current.id)
                break;
        }
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        handleEdit()
    }

    return (
        <EditableComponentWrapper
            ref={containerRef}>
            {list.map((item)=> {
                if(item.id == editMode || item.text) {
                    return (
                        <EditableComponent
                            ref={ ref }
                            onDragStart={(e) => handleDragStart(e)}
                            onDragEnd={(e) => handleDragEnd(e)}
                            onDragOver={(e) => e.preventDefault()}
                            onClick={() => setEditMode(item.id)} 
                            onBlur={() => handleEdit()}
                            theme={theme}
                            key={item.id}
                            draggable>
                            {editMode !== item.id ? 
                                <span>{item.text}</span> :
                                <form onSubmit={(e) => handleSubmit(e)}>
                                    <Input
                                        key={`${item.id}-input`}
                                        onChange={(value) => handleChange({ id: item.id, text: value })}
                                        value={item.text || current.text}/>
                                </form>
                            }
                        </EditableComponent>
                    )
                }
            })} 
        </EditableComponentWrapper>
    )
}

ContentEditable.propTypes = {
    addNewItem: PropTypes.bool,
    list: PropTypes.array.isRequired,
    current: PropTypes.object,
    theme: PropTypes.object
};
export default ContentEditable