import React from 'react';
import { shallow } from 'enzyme';
import ContentEditable from './ContentEditable';
import primaryTheme from '../../theme/primaryTheme';
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'

const theme={primaryTheme}

test('should render the ContentEditable correctly', () => {
    const initialState = {}
    const mockStore = configureStore()
    const store = mockStore(initialState)
    const current = {id:1,text:'test'}
    const list = [{id:1,text:'test'},{id:2,text:'test2'}]
    const addNewItem = false
    const wrapper = shallow(
                            <Provider store={store}>
                                <ContentEditable 
                                    current={current} 
                                    list={list} 
                                    addNewItem={addNewItem} 
                                    theme={theme}/>
                            </Provider>
                            );
    expect(wrapper).toMatchSnapshot();
});
