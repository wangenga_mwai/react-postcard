import {ADD_ITEM, UPDATE_CURRENT, DELETE_ITEM} from '../actionTypes'
export const addItem = (item) => ({ type: ADD_ITEM, payload: item });
export const updateCurrent = (current) => ({ type: UPDATE_CURRENT, payload: current });
export const deleteItem = (id) => ({ type: DELETE_ITEM, payload: { id } });