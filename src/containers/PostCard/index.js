import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import styled from 'styled-components';

import primaryTheme from '../../theme/primaryTheme';
import Button from '../../components/Button/Button';
import ContentEditable from '../../components/ContentEditable/ContentEditable';
import img from '../../img/bg-1.png';

const PostCardWrapper = styled.div`
  text-align: center;
  text-align: center;
  background: white;
  width: 80%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  padding: 50px;
  max-width: 824px;
  max-height: 626px;
`;

const PostcardContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${props => props.background};
  transition: all 0.3s ease-out;
  width: 75%;
  position: relative;
  &:after {
    content: '';
    position: absolute;
    right: 0;
    height: 110%;
    border: 1px solid ${props => props.bordercolor}
  }
`;

const BackgroundContainer = styled.div`
  display:flex;
  width: 326px;
  height: 484px;
  transition: all 0.5s ease-out;
  overflow: hidden;
  position: relative;
  transform: rotate(${props => props.rotation}deg);
  &:after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: -1;
    background-image: url(${img});
    background-size: 100% 100%;
    transition: all 0.5s ease-in-out;
    transform: scale(${props => props.iszoomedin ? '1.5' : '1.0'});
}
`;
const BackgroundImageSelector = styled.div`
  align-items: center; 
  justify-content: center;
`;
const NavigationContainer = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  flex-direction: column;
  align-self: flex-start;
  align-items: center;
  margin-top: 10px;
  width: 25%;
`;

const PostCard = () => {
    const [addNewItem, setNewTextArea] = useState(false);
    const [postcardRotation, setPostcardRotation] = useState(0);
    const [isZoomedIn, toggleZoom] = useState(false);
    const list = useSelector(state => state.list);
    const current = useSelector(state => state.current);
    const addNewTextarea = () => {
      setNewTextArea(true);
    };

    useEffect(() => {
      if (addNewItem) {
        setNewTextArea(false);
      }
    }, [addNewItem]);
    const rotatePostcard = () => {
      setPostcardRotation(postcardRotation + 90)
    }
    return (
        <PostCardWrapper>
            <PostcardContainer 
              background={primaryTheme.colorWhite} 
              bordercolor={primaryTheme.colorGrayLight}>
                <BackgroundContainer 
                  iszoomedin={isZoomedIn}
                  rotation={postcardRotation}>
                    <ContentEditable 
                      current={current}
                      list={list} 
                      addNewItem={addNewItem}
                      theme={primaryTheme}/>
                </BackgroundContainer>
                <BackgroundImageSelector></BackgroundImageSelector>
            </PostcardContainer>
            <NavigationContainer>
                <Button onClick={() => addNewTextarea()} theme={primaryTheme}>New Text Block</Button>
                <Button onClick={() => {toggleZoom(true)}} theme={primaryTheme}>Zoom In</Button>
                <Button onClick={() => {toggleZoom(false)}} theme={primaryTheme}>Zoom Out</Button>
                <Button onClick={() => rotatePostcard()} theme={primaryTheme}>Rotate</Button>
            </NavigationContainer>
        </PostCardWrapper>
    )
}

export default PostCard