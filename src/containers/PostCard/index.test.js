import React from 'react'
import Enzyme, {shallow, mount} from 'enzyme'
import PostCard from './index'
import primaryTheme from '../../theme/primaryTheme'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import ContentEditable from '../../components/ContentEditable/ContentEditable'

Enzyme.configure({ adapter: new Adapter() });

const theme={primaryTheme}

describe('Test Basic functionality', () => {
    const initialState = {
        current: {},
        list: [{id:1,text:'test'},{id:2,text:'test2'}]
    }
    const mockStore = configureStore()
    const store = mockStore(initialState)
    test('should render the PostCard correctly', () => {
        const wrapper = shallow(
                                <Provider store={store}>
                                    <PostCard/>
                                </Provider>
                                );
        expect(wrapper).toMatchSnapshot();
    });
    it('Test new text block add works', () => {
        const wrapper = mount(
            <Provider store={store}>
                <PostCard/>
            </Provider>
            );
        wrapper.find('button').at(0).simulate('click');
        expect(wrapper.find(ContentEditable)).toHaveLength(1)
    });
});
