import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'

it('renders without crashing', () => {
  const initialState = {
    list: [{id: 1, text: "sunny day"}]
  };
  const mockStore = configureStore()
  const store = mockStore(initialState)
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}><App /></Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
