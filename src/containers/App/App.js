import React from 'react';
import { MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import PostCard from '../PostCard';

const THEME = createMuiTheme({
    typography: {
        "fontFamily":"Lato",
        "fontWeight": "400",
        "fontSize": 16,
        "lineHeight": "1.7"
    }
});

const App = () => (
    <CssBaseline>
        <MuiThemeProvider theme={THEME}>
            <PostCard/>
        </MuiThemeProvider>
    </CssBaseline>
)

export default App;
