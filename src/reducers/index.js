import {unionBy} from 'lodash';
import {ADD_ITEM, UPDATE_CURRENT, DELETE_ITEM} from '../actionTypes'

const initialState = {
    list: [{id: 1, text: "sunny day"}]
};

const reducer = (state = initialState, action) => {
    console.log(action);
    switch (action.type) {
        case ADD_ITEM:
            return { ...state, list: unionBy([action.payload], state.list, 'id') };
        case UPDATE_CURRENT:
            return { ...state, list: unionBy([action.payload], state.list, 'id') };
        case DELETE_ITEM:
            return { ...state, list: state.list.filter(item => item.id !== action.payload.id) }
        default:
            return state;
    }
}

export default reducer;