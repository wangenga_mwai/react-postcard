const primaryTheme = {
    colorPrimary: '#666666',
    colorSecondary: '#E5E5E5;',
    colorTertiary: '#D0D0D0',
    colorGrayLight: '#D2D2D2',
    colorGrayMedium: '#808080',
    colorGrayDark: '#333333',
    colorWhite: '#FFFFFF',
    colorBlue: '#63ABFF'
};


export default primaryTheme