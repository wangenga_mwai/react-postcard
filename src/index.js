import React from 'react';
import {render} from 'react-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from './reducers';

import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';
import {createGlobalStyle} from 'styled-components'
import 'typeface-lato';

const GlobalStyle = createGlobalStyle`
    *,
    *::after,
    *::before {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    }

    html {
      margin: 0px;
      height: 100%;
      width: 100%;
    }

    body {
      box-sizing: border-box;
      font-family: "Lato", sans-serif;
      font-weight: 400;
      font-size: 16px;
      line-height: 1.7;
      color: #333333;
      padding: 0;
      min-height: 100%;
      width: 100%;
    }
    #root {
      height: 100vh;
      width: 100vw;
    }
`;

const store = createStore(reducer);

render(
  <Provider store={store}>
      <GlobalStyle />
      <App />
  </Provider>
  , document.getElementById('root'));
registerServiceWorker();
