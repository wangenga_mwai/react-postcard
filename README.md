# Development Plan: 
  - Requirement Analysis (Task and tool breakdown)
  - Architecture Selection:
    - Tool & Tech Setup
    - Component Breakdown
    - Feature Specification
# Estimated Efforts: Time estimate (5hrs); including setup
  - Determine components & component actions
  - Mockup component structure & test requirement
  - Setup tools & initial app structure
  - Build out components
  - Add actions ie. Basic functionality
  - Manual testing
  - Unit testing

# Architecture
  - Framework: React.js
  - Preprocessor: node-sass-chokidar
  - Styling: Styled Components with Material-UI
  - Tests: Jest x Enzyme
  - State Mngmt: Redux
  - File & component generator: Plop-js
# Suggestions for Improvement
  - Add key navigation
    - Back & forward to rotate
    - Double click to zoom
    - Subtle Animation to rotate & zoom
# Test / Pass criteria
  - Allow for a new text block addition (Max:5)
  - Allow to rotate in either direction
  - Zoom / exit works as expected
  - Undo & redo navigates to the correct action

# Running the App
  - `npm/yarn install`: Initial setup
  - `npm start`: Run app
  - `npm run test`: Run tests